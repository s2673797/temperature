package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getFer(String string) {
        //Converting Celcious from string to double
        double a = Double.parseDouble(string);
        //Converting Celicous to Ferh
        //Formula: (celcious × 9/5) + 32 = Farh.
        double convertion = ((a * (1.8)) + 32);
        //Putting Cel as the key and Fah as the value
        //Returing the corspending Farh to of a cel
        return convertion;
    }
}
